#!/usr/bin/env php
<?php

$nl = 1;
$conteudo = [];

if (!isset($argv[1])) {
    echo "Arquivo não informado!\nSaindo...\n\n";
    exit(1);
}

while (true) {
    $line = readline(str_pad($nl, 3, '0', STR_PAD_LEFT).': ');
    if ($line != '') {
        $conteudo[] = $line;
        $nl++;
    } else {
        break;
    }
}

if (empty($conteudo)) {

    echo "\nNada para salvar!\nSaindo...\n\n";
    exit(1);

} else {

    if (file_exists($argv[1])) {

        $h = fopen($argv[1], 'a+');
        foreach ($conteudo as $linha) {
            fwrite($h, $linha."\n");
        }
        fclose($h);

    } else {

        file_put_contents($argv[1], implode("\n", $conteudo)."\n");

    }

}

