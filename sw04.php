<?php


if (!isset($argv[1])) goto d;

switch($argv[1]) {
    case 1:
        goto a;
    case 2:
        goto b;
    default:
        goto c;
}


a: {
    echo "banana\n";
    echo "laranja\n";
    goto c;
}

b: {
    echo "limão\n";
    echo "abacaxi\n";
    goto c;
}


c:

exit(0);

d:

echo "Nenhum parâmetro passado!\n";

goto c;
