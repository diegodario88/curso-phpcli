# 1 - Conceitos básicos

O PHP-CLI é essencialmente o mesmo PHP utilizado no desenvolvimento de aplicações para web. A única diferença é que ele é capaz de interagir diretamente com a linha de comandos, tornando-se uma poderosa alternativa para a criação de scripts e programas para uso no terminal e até no desktop.

> **Importante!** O PHP é um Software Livre!

Neste curso, nós veremos as características especiais do PHP-CLI e partiremos diretamente para os principais fundamentos da linguagem PHP.


## 1.1 - Instalação e módulos essenciais

A maioria das distribuições GNU/Linux fornecem o PHP-CLI em seus repositórios oficiais com o nome de `php-cli`. O que pode variar são as versões e os módulos instalados por padrão.

No Debian Stable (atualmente na versão 10 Buster), o pacote `php-cli` instala a versão 7.3.14 e depende dos seguintes pacotes:

```
php7.3-cli
libedit2 (>= 2.11-20080614-4)
libmagic1
mime-support
7.3-common (= 7.3.14-1~deb10u1)
php7.3-json
php7.3-opcache
php7.3-readline
tzdata
ucf
libargon2-1 (>= 0~20171227)
libc6 (>= 2.27)
libpcre2-8-0 (>= 10.32)
libsodium23 (>= 1.0.14)
libssl1.1 (>= 1.1.0)
libxml2 (>= 2.8.0)
zlib1g (>= 1:1.1.4)
```

Complementarmente, nós sugerimos a instalação dos seguintes módulos do PHP:

```
php-fpm
php-pdo
php-mysql
php-zip
php-gd
php-mbstring
php-curl
php-xml
php-pear
php-bcmath
```

> **Nota:** Consulte a documentação da sua distribuição sobre como instalar o PHP-CLI e os módulos sugeridos.

## 1.2 - Opções da linha de comando

Com o PHP-CLI instalado, você terá o comando `php` à disposição para executar scripts, obter informações sobre o PHP, trabalhar no modo interativo e até iniciar um servidor web embutido para testar suas aplicações web!

As opções do comando `php` são muitas, mas nós veremos as mais interessantes para o nosso curso.

### Listando as opções

Para listar as opções de linha de comando do PHP, execute:

```
:~$ php -h
```

Para uma ajuda mais completa, nós também podemos consultar a página do manual:

```
:~$ man php
```

### Opções de execução de código

Para executar um script em PHP, basta usar o comando:

```
:~$ php <arquivo do script>
```

Nós também podemos executar um código em PHP diretamente na linha de comando:

```
:~$ php <<< '<?php echo "Olá, mundo!\n"; ?>'
Olá, mundo!
:~$
```

Como o comando `php` espera um arquivo, nós precisamos utilizar um recurso do shell chamado ***here string*** (`<<<`), que equivale basicamente ao comando do shell:

```
:~$ echo '<?php echo "Olá, mundo!\n"; ?>' | php
Olá, mundo!
:~$
```

Contudo, é possível evitar a *here string* e as tags do PHP com a opção `-r`:

```
php -r 'echo "Olá, mundo!\n";'
Olá, mundo!
:~$
```

Em ambos os casos, é importante observar que o código deve estar entre aspas simples (para evitar as expansões do shell) e deve obrigatoriamente terminar com `;`.

### Obtendo informações sobre o PHP

Para obter todas uma lista completa de informações sobre o PHP, basta executar:

```
:~$ php -i
```

Como a lista apresentada é muito longa, nós podemos restringir a exibição aos termos que nos interessam com o utilitário do sistema `grep`. Por exemplo:

```
:~$ php -i | grep -i memory
memory_limit => -1 => -1
Collecting memory statistics => No
opcache.memory_consumption => 128 => 128
opcache.preferred_memory_model => no value => no value
opcache.protect_memory => 0 => 0
```

> **Nota:** A sintaxe `grep -i` faz o `grep` ignorar diferenças entre caixa alta e baixa na busca.

Também podemos ver uma lista dos módulos do PHP que estão instalados:

```
:~$ php -m
```

Mas, se quisermos apenas a versão do PHP, é só executar:

```
:~$ php -v
PHP 7.3.14-1~deb10u1 (cli) (built: Feb 16 2020 15:07:23) ( NTS )
Copyright (c) 1997-2018 The PHP Group
Zend Engine v3.3.14, Copyright (c) 1998-2018 Zend Technologies
    with Zend OPcache v7.3.14-1~deb10u1, Copyright (c) 1999-2018, by Zend Technologies
```

## 1.3 - O modo interativo

Uma das opções mais interessantes do comando `php` é aquela que nos permite acessar um shell interativo. Nele, nós podemos executar e testar códigos em PHP diretamente no terminal:

```
:~$ php -a
Interactive mode enabled

php > echo "Olá, mundo!\n";
Olá, mundo!
php > 

```

Para sair do modo interativo, nós utilizamos o comando `exit`.

Ao longo do curso, boa parte dos elementos da linguagem PHP será demonstrada no modo interativo antes de escrevermos nos nossos scripts.

## 1.4 - O servidor embutido

Outra opção poderosíssima do comando `php` é o servidor web embutido (*built in*).

```
:~$ php -S localhost:8000
```

> **Nota:** Mesmo não sendo o foco do nosso curso, vale a pena mencionar que não precisamos instalar um servidor para testar nossas aplicações para a web. 

## 1.5 - Como executar os scripts

Como vimos, os scripts em PHP podem ser executados no terminal pelo comando:

```
:~$ php <arquivo do script>
```

Mas isso só é necessário se o arquivo não for executável e não possuir uma linha indicando o interpretador de comandos. 

A linha do interpretador, ou "*shebang*", deve ser a primeira linha do script e serve para iniciar uma sessão do interpretador, que no nosso caso é o `php`:

```
#!/usr/bin/env php
```

Para tornar o arquivo do script executável, nós usamos o comando `chmod` com a opção `+x`:

```
:~$ chmod +x <arquivo do script>
```