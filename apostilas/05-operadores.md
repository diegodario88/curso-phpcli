# PHP-CLI - Aula 5: Operadores

Os operadores são elementos que pegam uma expressão e geram um valor, de modo que a própria operação torna-se uma expressão. Um exemplo disso é o operador de atribuição `=`, utilizado para atribuir o valor de uma expressão a uma variável:

```
$a = 10;
```

Após esta operação ser executada, tanto `$a` quanto `10` (uma expressão literal de valor 10) e `a = 10` são expressões que passam a representar o valor 10.

Se agruparmos os operadores de acordo com o número de valores envolvidos nas operações que eles realizam, nós veremos que, no PHP, eles podem ser de três tipos:

* **Unários** - Operadores que recebem apenas um valor.
* **Binários** - Quando recebem dois valores. 
* **Ternário** - O único operador do PHP que recebe três valores.

## 5.1 - Prececência e agrupamento de operadores

Quando trabalhamos com vários operadores numa expressão, é fundamental compreender em que ordem o interpretador irá avaliá-los antes de realizar as operações que escrevermos em nossos códigos. Por exemplo:

```
2 + 7 * 2
```

Essa expressão, como aprendemos na matemática, resultará no valor 16, não em 18, porque a multiplicação é efetuada antes da soma, ou seja, a multiplicação tem **precedência** sobre a soma. 

Do mesmo modo, quando dois ou mais operadores de precedência igual aparecem na mesma expressão, o interpretador irá avaliar como eles podem ser agrupados, ou até se eles podem ser agrupados. Outro exemplo, ainda com o operador de adição:

```
2 + 3 + 4
```

Aqui, o operador de adição pode ser agrupado em pares de termos da esquerda para a direita. Portanto, a primeira soma à esquerda será efetuada e seu resultado será somado ao último termo da operação. Já os operadores de atribuição (`=`) são agrupados da direita para a esquerda:

```
$a = $b = 10
```

Neste exemplo, o valor da expressão literal `10` é atribuída à variável `$b` antes do valor da expressão `$b` ser atribuído à variável `$a`.

Uma forma simples de controlar a precedência dos operadores (e poupar dores de cabeça) é através do uso dos parêntesis. Então, voltando ao primeiro exemplo, se quisermos que a soma seja feita antes da multiplicação, nós escrevemos:

```
(2 + 7) * 2
```

O que, desta forma, resultaria em 18.

Contudo, existem tipos de operadores que não podem aparecer juntos na mesma expressão, os chamados operadores não associáveis, como é o caso dos:

* Operadores de comparação;
* Operadores de incremento e decremento;
* Operadores de tipo (casts);
* Operadores ligados à instanciação de novos objetos;
* Operadores de funções geradoras (yield);
* O operador de controle de erro (`@`);
* E do operador de negação lógica (`!`).

São agrupados da direita para a esquerda:

* O operador de potenciação (`**`);
* O operador de coalescência nula (`??`);
* E os operadores de atribuição.

Todos os demais, se forem associáveis, serão agrupados da esquerda para a direita.
> **Importante!** A precedência e a associatividade dos operadores só determinal como as expressões serão agrupadas, mas o PHP geralmente não especifica a ordem exata em que elas serão interpretadas, o que pode nos levar a situações ambíguas, por exemplo:

```
php > $x = 4;
php > echo "4 - 1 = " . $x - 1 . ", ou não?\n";
PHP Notice:  A non well formed numeric value encountered in php shell code on line 1
3, ou não?
```

Repare que, como os operadores `-` e `.` têm a mesma precedência, o PHP efetuou primeiro a concatenação da string `4 - 1 =` com o valor em `$x` para, somente depois, tentar realizar a subtração. Mas, neste ponto, `$x` já não estava disponível como um valor numérico, o que gerou a mensagem de erro.

É por isso que, por menos que seja necessário, é uma ótima ideia utilizar sempre os parêntesis:

```
php > $x = 4;
php > echo "4 - 1 = " . ($x - 1) . ", ou não?\n";
4 - 1 = 3, ou não?
```

## 5.2 - Operadores aritméticos

Os operadores aritméticos são essencialmente os mesmos utilizados nas quatro operações básicas mais o módulo, a potenciação e os sinais de negação e identidade.

| Operador   | Nome          | Resultado |
|:----------:|---------------|-----------|
| `+$a`      | Identidade    | Converte `$a` para um valor do tipo numérico. |
| `-$a`      | Negação       | Converte `$a` para um valor do tipo numérico e inverte o sinal. |
| `$a + $b`  | Soma          | Soma o valor de `$a` com o valor de `$b`. |
| `$a - $b`  | Subtração     | Subtrai de `$a` o valor de `$b`. |
| `$a * $b`  | Multiplicação | Multiplica o valor de `$a` pelo de `$b`. |
| `$a / $b`  | Divisão       | Divide `$a` por `$b`. |
| `$a % $b`  | Módulo        | Resto da divisão de `$a` por `$b`. |
| `$a ** $b` | Potenciação   | Eleva o valor de `$a` à potência `$b`. |

**Observações:**

* Se a divisão resultar em um valor com casas decimais, o quociente será um valor do tipo `float`, a menos que os dois operandos sejam inteiros e divisíveis.

* Para efetuar divisões e ter sempre um inteiro no resultado (obviamente truncado na parte inteira), nós podemos utilizar a função `intdiv()`.

* Os operandos do módulo são truncados e convertidos para inteiros antes da operação ser efetuada. 

* Para obter o módulo de operandos contendo valores do tipo `float`, nós podemos utilizar a função `fmod()`.

* O sinal do módulo será o mesmo do dividendo (o primeiro termo da operação).

## 5.3 - Operadores de atribuição

São os operadores que atribuem ao termo da esquerda o valor da expressão da direita. Como resultado, a própria expressão da atribuição assume o valor atribuído, o que permite fazer coisas como:

```
php > $a = ($b = 5) + 7;
php > echo $a;
12
```

### 5.3.1 - Operadores de atribuição combinados

Além do operador básico de atribuição (`=`), existem operadores combinados para todas as operações aritméticas e os operadores de strings e de união de arrays:

| Operador         | Equivalência            |
|:----------------:|-------------------------|
| `$a += $b`       | `$a = $a + $b`          |
| `$a -= $b`       | `$a = $a - $b`          |
| `$a *= $b`       | `$a = $a * $b`          |
| `$a /= $b`       | `$a = $a / $b`          |
| `$a %= $b`       | `$a = $a % $b`          |
| `$a **= $b`      | `$a = $a ** $b`         |
| `$a .= $b`       | `$a = $a . $b`          |
| `$arr1 += $arr2` | `$arr1 = $arr1 + $arr2` |

### 5.3.2 - Operadores de incremento e decremento

Os operadores de incremento e decremento correspondem aos operadores de atribuição combinados `+=` e `-=` seguidos do valor `1`. Contudo, os operadores de incremento e decremento oferecem uma opção a mais quanto ao momento em que a operação será efetuada: antes ou depois do retorno do valor processado.

| Operador | Nome           | Descrição                                         |
|:--------:|----------------|---------------------------------------------------|
| `$a++`   | Pós incremento | Retorna o valor de `$a` e depois soma 1.          |
| `++$a`   | Pré incremento | Soma 1 ao valor de `$a` e retorna o resultado.    |
| `$a--`   | Pós decremento | Retorna o valor de `$a` e depois subtrai 1.       |
| `--$a`   | Pré decremento | Subtrai 1 do valor de `$a` e retorna o resultado. |

## 5.4 - Operadores bit-a-bit (*bitwise*)

São os operadores que permitem a manipulação e a interpretação de bits específicos de valores inteiros.

| Operador | Nome | Descrição |
|:--------:|------|-----------|
| `$a & $b` | E (*and*) | Bits ativos simultaneamente em `$a` e `$b` são os bits ativos no inteiro resultante. |
| `$a \| $b` | OU (*or*) | Bits ativos em `$a` ou `$b` são os bits ativos no inteiro resultante. |
| `$a ^ $b` | OU exclusivo (*xor*) | Bits ativos em `$a` ou `$b`, mas não em ambos, são os bits ativos no inteiro resultante. |
| `~ $a` | NÃO (not) | Inverte o bits ativos e inativos em `$a`. |
| `$a << $b` | Deslocamento para esquerda | Desloca os bits de `$a` `$b` passos para a esquerda. |
| `$a >> $b` | Deslocamento para direita | Desloca os bits de `$a` `$b` passos para a direita. |

**Observações**

* O deslocamento de bits no PHP é aritmético.

* Somente deslocamentos para a direita preservam o bit de sinal.

* Se ambos os termos das expressões com `&` `|` e `^` forem strings, os operadores irão considerar os valores em ASCII dos caracteres.

* Se o operador `~` for usado com uma string, a operação será executada com os valores ASCII dos caracteres.

* Os operadores `<<` e `>>` sempre tratarão os termos da operação como inteiros.

* Ao utilizar qualquer um desses operadores, devemos nos lembrar de que as operações são realizadas com os bits de valores do tipo inteiro, o que implica na existência de vários bits com estado `0` antes da representação binária dos valores envolvidos.

Por exemplo:

```
php > $v = 5;
php > printf("%b", $v);
101
```

Na conversão para binário, o `printf()` ocultou todos os bits antes do primeiro bit `1`, já que eles estão inativos (`0`). Mas, se utilizarmos o operador NÃO (`~`)...

```
php > printf("%b", ~$v);
1111111111111111111111111111111111111111111111111111111111111010
```

Como podemos ver, todos os bits inativos do inteiro `5` foram ativados e vice-versa, o que resultaria no inteiro `-6`, e não no decimal `10`. Isso acontecepor dois motivos:

1. O PHP não trabalha com inteiros sem sinal.
2. Binários iniciados com bit 1 representam valores negativos.

## 5.5 - Operadores de comparação

São os operadores utilizados para comparar dois valores e gerar um valor verdadeiro (`true`) ou falso (`false`) de acordo com a verdade da afirmação feita na expressão.

| Operador | Afirmação | Descrição |
|:----------:|------|-----------|
| `$a == $b` | É igual | Verdadeiro se `$a` for igual a `$b` após o ajuste de seus tipos. |
| `$a === $b` | É idêntico | Verdadeiro se `$a` for estritamente igual a `$b`, inclusive no tipo. |
| `$a != $b` | É diferente | Verdadeiro se `$a` for diferente de `$b` após o ajuste de seus tipos. |
| `$a <> $b` | É diferente | Verdadeiro se `$a` for diferente de `$b` após o ajuste de seus tipos. |
| `$a !== $b` | Não é idêntico | Verdadeiro se `$a` for diferente de `$b` ou se seus valores forem de tipos diferentes. |
| `$a < $b` | É menor | Verdadeiro se `$a` for menor do que `$b`. |
| `$a > $b` | É maior | Verdadeiro se `$a` for maior do que `$b`. |
| `$a <= $b` | É menor ou igual | Verdadeiro se `$a` for menor ou igual a `$b`. |
| `$a >= $b` | É maior ou igual | Verdadeiro se `$a` for maior ou igual a `$b`. |
| `$a <=> $b` | "Nave espacial" | Retorna um inteiro maior, menor ou igual a zero se `$a` for respectivamente maior, menor ou igual a `$b`. |

### 5.5.1 - O operador ternário

Um tipo especial de operador de comparação é o **operador ternário**, que funciona mais ou menos como uma forma abreviada da declaração `if`.

```
(EXPRESSÃO1) ? (EXPRESSÃO2) : (EXPRESSÃO3)
```

Aqui, EXPRESSÃO2 será efetuada se a EXPRESSÃO1 for verdadeira. Caso seja falsa, a EXPRESSÃO3 é que será efetuada. Por exemplo:

```
php > $a = 1;
php > $b = ($a > 5) ? 5 : $a++;
php > echo $b;
1
```

Alternativamente, podemos ocultar a expressão do meio:

```
(EXPRESSÃO1) ?: (EXPRESSÃO3)
```

Assim, se EXPRESSÃO1 for verdadeira, seu valor será o resultado do ternário. Caso contrário, será o valor de EXPRESSÃO3:

```
php > $a = 3;
php > $b = ($a > 5) ?: $a++;
php > var_dump($b);
int(3)
php > $a = 10;
php > $b = ($a > 5) ?: $a++;
php > var_dump($b);
bool(true)
```

### 5.5.2 - O operador de coalescência nula

A partir do PHP 7, nós temos mais um operador muito interessante, com o qual nós podemos atribuir um valor padrão a uma variável: `??`, o operador de coalescência nula.

```
(EXPRESSÃO1) ?? (EXPRESSÃO2)
```

Aqui, a EXPRESSÃO2 será efetuada se a EXPRESSÃO1 for nula, o que pode ser muito útil para a definição de valores alternativos, caso o elemento de uma array não esteja definido:

```
php > $par = $a[1] ?? "Não tem...";
php > echo $par;
Não tem...
```

Ou então, em um script (`teste`)...

```
#!/usr/bin/env php
<?php

function erro() {
    echo "Você errou!\n";
    exit(25);
}

$a = $argv[1] ?? erro();

echo "$a - Tudo certo!\n";
```

Executando...

```
:~/cursos/php-cli/09$ ./teste 
Você errou!
:~/cursos/php-cli/09$ ./teste olá
olá - Tudo certo!
```

## 5.6 - Operadores lógicos

Diferente dos operadores de comparação, que avaliam afirmações sobre as expressões e retornam o valor verdade resultante, os operadores lógicos geram um valor verdadeiro (`true`) ou falso (`false`) de acordo com a condição de verdade de cada termo da expressão. Ou seja, eles irão comparar o estado lógico de cada expressão participante, não a correspondência entre elas.

| Operador     | Nome      | Descrição                                                             |
|:------------:|-----------|-----------------------------------------------------------------------|
| `$a and $b`  | E (*and*) | Retorna `true` se `$a` e `$b` forem `true`.                           |
| `$a or $b`   | OU (*or*) | Retorna `true` se `$a` ou `$b` forem `true`.                          |
| `$a xor $b`  | OU exclusivo (*xor*) | Retorna `true` se `$a` ou `$b` forem `true, mas não ambos. |
| `! $a`       | NÃO (not) | Retorna `true` se `$a` for `false`.                                   |
| `$a && $b`   | E (*and*) | Retorna `true` se `$a` e `$b` forem `true`.                           |
| `$a \|\| $b` | OU (*or*) | Retorna `true` se `$a` ou `$b` forem `true`.                          |

> **Observação:** Os operadores `||` e `&&` têm precedência sobre seus equivalentes `or` e `and`.


## 5.7 - Operador de execução

Especialmente interessante para nós, que estamos estudando o PHP-CLI, é o operador de execução, que permite executarmos comandos do shell e atribuirmos suas saídas a variáveis. 

```
`COMANDOS DO SHELL`
```

O funcionamento do operador de execução é idêntico ao da função `shell_exec()`.

Exemplo:

```
php > $user = `whoami`;
php > echo $user;
gda
```

