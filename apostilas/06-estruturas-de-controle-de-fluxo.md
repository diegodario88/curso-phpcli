# PHP-CLI - Aula 6: Estruturas de controle de fluxo

Como vimos no tópico anterior, alguns operadores podem se comportar como estruturas de decisão. É o caso do **operador ternário** e do **operador de coalescência nula** que, de acordo com uma condição, são capazes de realizar atribuições de valores:

**Operador ternário**

```
$VAR = (CONDIÇÃO) ? VALOR_SE_TRUE : VALOR_SE_FALSE;

$VAR = (CONDIÇÃO) ?: VALOR_SE_FALSE;
```

**Operador de coalescência nula**

```
$VAR = (EXPRESSÃO1) ?? (EXPRESSÃO_SE_EXPRESSÃO1_FOR_NULA);
```

As estruturas de controle de fluxo fazem a mesma coisa, só que com blocos inteiros de instruções de todo tipo: atribuições, funções, *loops*, declarações condicionais, etc... O agrupamento de instruções no PHP é feito através das chaves (`{...}`). O mais importante aqui, é que todo bloco de instruções agrupado entre chaves também é uma instrução.

No PHP, nós poderíamos classificar esses blocos em três grupos principais:

* Estruturas de decisão
* Estrututras de repetição
* Operador de salto (goto)

Todas essas estruturas são construtoras de linguagem, todas podem anteceder grupos de instruções entre chaves e, exceto no caso do operador `goto`, todas as demais podem ser chamadas de **declarações** ou, como é mais comum e preciso em português, **cláusulas** (pense em "cláusulas de um contrato").

## 6.1 - As cláusulas 'if/elseif/else'

Presente em diversas linguagens, a cláusula `if` permite a execução condicional de blocos de código. Na sua forma básica, uma estrutura `if` seria escrita desta forma:

```
if (EXPRESSÃO_TRUE) INSTRUÇÃO;
```

Onde EXPRESSÃO_TRUE é qualquer expressão avaliada como verdadeira. Caso a expressão resulte em `false`, a INSTRUÇÃO é ignorada. Por exemplo:

```
php > $a = 10;
php > if ($a < 15) echo "É menor do que 15!";
É menor do que 15!
```

Ou então:

```
php > $a = 20;
php > if ($a < 15) echo "É menor do que 15!";
php > 
```

Onde, neste caso, a instrução foi ignorada.

Observe, porém, que esta cláusula `if` está condicionando a execução de apenas uma instrução. Tudo que vier depois dela no código, se ainda houver algo a ser executado, não está condicionado à expressão avaliada:

```
php > $a = 10;
php > if ($a < 15) echo "É menor!\n"; echo "E a vida segue...";
É menor!
E a vida segue...

php > $a = 20;
php > if ($a < 15) echo "É menor!\n"; echo "E a vida segue...";
E a vida segue...
```

Neste exemplo, a segunda instrução `echo` foi executada de forma independente da condição estabelecida pelo `if`. Se a ideia fosse condicionar a instrução seguinte à avaliação da mesma condição, digamos, para executar algo caso a expressão seja avaliada como falsa, nós poderíamos utilizar a instrução `else`:

```
php > // A expressão é verdadeira...
php > $a = 10;
php > if ($a < 15) echo "Menor!\n"; else echo "Maior!\n"; echo "Seguindo...";
Menor!
Seguindo...

php > // A expressão é falsa...
php > $a = 20;
php > if ($a < 15) echo "Menor!\n"; else echo "Maior!\n"; echo "Seguindo...";
Maior!
Seguindo...
```

A instrução `else` depende da ocorrência de uma cláusula `if` (ou `elseif`, como veremos adiante) imediatamente antes, ou não será executada. O exemplo abaixo resultaria em um erro de sintaxe:

```
php > else echo "Foi?";
PHP Parse error:  syntax error, unexpected 'else' (...)
```

A instrução `else` também pode vir acompanhada de uma segunda condição declarada com um `if`, resultando numa instrução `else if` ou, de forma abreviada, `elseif`:

```
php > // A expressão em 'else if' é a verdadeira...
php > $a = 15;
php > if ($a < 10) echo "Menor\n";
php > else if ($a > 10) echo "Maior\n"; 
Maior
php > else echo "Igual\n";
php > 
```

### 6.1.1 - Agrupando instruções

Embora essa sintaxe seja muito prática para condicionarmos a execução de apenas uma instrução a cada uma das cláusulas `if`, `elseif` e `else`, é bastante comum nós precisarmos executar um grupo de várias instruções, e é aí que entram as chaves.

Talvez a sintaxe mais conhecida de uma estrutura `if/elseif/else` seja aquela com as instruções agrupadas entre chaves, mas isso só é realmente obrigatório quando precisamos desses agrupamentos:

```
if (EXPRESSÃO_TRUE 1) {

    INSTRUÇÕES...

} elseif (EXPRESSÃO_TRUE 2) {

    INSTRUÇÕES...

} elseif (EXPRESSÃO_TRUE N) {

    INSTRUÇÕES...

} else {

    INSTRUÇÕES...

}
```

### 6.1.2 - Uma sintaxe alternativa

Uma sintaxe alternativa pouco conhecida é aquela que substitui a abertura das chaves por dois pontos (`:`) e o fechamento do bloco por um `endif;`:

```
if (EXPRESSÃO_TRUE 1):

    INSTRUÇÕES...

elseif (EXPRESSÃO_TRUE 2):

    INSTRUÇÕES...

elseif (EXPRESSÃO_TRUE N):

    INSTRUÇÕES...

else:

    INSTRUÇÕES...

endif;
```

> **Importante!** com esta sintaxe, o `elseif` não pode ser utilizado na sua forma separada (`else if`)!

Apesar de lembrar as estruturas na linguagem Python, observe que aqui as tabulações são meramente estéticas, tanto que isso é possível:

```
php > $a  = true;
php > if ($a): echo "bom\n"; echo "dia\n"; else: echo "falso\n"; endif;
bom
dia
```

### 6.1.3 - Uma dica especial

Esta é uma forma simples e prática de exibir mensagens condicionada à uma variável estar ou não definida:

```
php > // $c não está definida...
php > isset($c) and print $c;

php > // Definindo $c...
php > $c = 23;
php > isset($c) && print $c;
23
```

Ou então...

```
php > $a = 15;
php > // $c não está definido...
php > isset($c) || print $a;
15
```

Ou, brincando mais um pouco com as expressões condicionais...

```
php > $a = 15;
php > // $c não está definido...
php > (isset($c) && print $c) || print $a;
15

php > // Definindo $c...
php > $c = 23;
php > (isset($c) && print $c) || print $a;
23
```

Se não quisermos usar parêntesis, nós podemos jogar com as precedências:

```
php > $a = 15;
php > // $c não está definido...
php > isset($c) && print $c or print $a;
15

php > // Definindo $c...
php > $c = 23;
php > isset($c) && print $c or print $a;
23
```

O princípio do funcionamento dessas expressões, apesar de parecer um mistério, é bem simples:

* Em uma operação lógica com o operador `&&` (ou `and`), se o primeiro termo é avaliado como `false` o valor de toda a expressão já está definido como `false`, e o segundo termo não precisa ser avaliado. Portanto, o `print` não chega a ser executado.

* Já numa operação lógica com o operador `||` (ou `or`), mesmo que o primeiro termo seja avaliado como `false`, a avaliação da expressão ainda dependerá do valor do segundo termo e, por isso, o `print` precisa ser executado. Por outro lado, se o primeiro termo for avaliado como `true`, a expressão inteira terá valor `true`, e o segundo termo não será avaliado.

## 6.2 - A estrutura 'switch/case'

De modo geral, o `switch` é similar a um conjunto de declarações `if/elesif` encadeadas onde uma mesma expressão é comparada com diversos valores:

```
if ($a == 1) {
    INSTRUÇÕES
} elseif ($a == 2) {
    INSTRUÇÕES
} elseif ($a == 3) {
    INSTRUÇÕES
} else {
    INSTRUÇÕES
}
```

Com a estrutura `switch/case`, o mesmo resultado seria obtido da seguinte forma:

```
switch($a) {
    case 1:
        INSTRUÇÕES
        break;
    case 2:
        INSTRUÇÕES
        break;
    case 3:
        INSTRUÇÕES
        break;
    default:
        INSTRUÇÕES
}
```

> **Nota:** A estrutura `switch/case` também suporta a forma alternativa sem chaves. Neste caso, o bloco deve ser encerrado com a palavra-chave `endswitch`.

A diferença entre as duas estruturas é que, no `switch`, a expressão é avaliada apenas uma vez, o que torna perfeitamente seguro avaliar o retorno de funções (internas ou do usuário) sem a preocupação com o fato delas terem que ser chamadas mais de uma vez. Obviamente, isso afeta positivamente o desempenho dos nossos programas.

Repare que, no equivalente `if/elseif`, nós utilizamos o operador `==`, de comparação flexível (que não considera os tipos). Isso porque o `switch` faz exatamente esse tipo de comparação, ou seja, o inteiro `0`, as strings e arrays vazias e o valor `false` serão tratados como equivalentes na comparação.

A cláusula `default`, ainda no exemplo acima, é um tipo especial de `case` que casa com qualquer valor da expressão em `switch`. Ela não é obrigatória, mas deve ser sempre utilizada como a última cláusula da estrutura.

É muito importante entender que a estrutura `switch/case` **executa linha por linha cada uma das instruções**. No começo, nenhum código é executado, o que acontece apenas quando uma cláusula `case` expressa um valor que encontra correspondência com o valor da expressão na cláusula `switch`. Neste ponto, e a partir daí, **todas as instruções subsequentes são executadas até o fim do bloco `switch`!**

Por este motivo, nós somos obrigados a utilizar a instrução `break` para interromper a execução do bloco `switch` no ponto em que queremos que isso conteça. Pode ser na própria cláusula `case` que encontrou correspondência ou em algum trecho mais adiante (depende do que queremos fazer), mas o `break` é a única forma de impedir a continuidade da execução. Por exemplo:

**Exemplo `sw01.php`**

```
$a = 1;

switch($a) {
    case 1:
        echo "1\n";
    case 2:
        echo "2\n";
    case 3:
        echo "3\n";
    default:
        echo "qualquer coisa!\n";
}
```

Executando, a nossa saída seria:

```
:~$ php sw01.php 
1
2
3
qualquer coisa!
```

Portanto, se esse comportamento não for o desejado, não podemos nos esquecer da instrução `break` ao final de cada bloco `case`.

Por outro lado, nós podemos aproveitar esse comportamento para definir mais de uma correspondência válida:

**Exemplo `sw02.php`

```
switch($argv[1]) {
    case 0:
    case 1:
    case 2:
        echo "0, 1 ou 2\n";
        break;
    case 3:
        echo "3\n";
        break;
    default:
        echo "qualquer coisa!\n";
}
```

Aqui, nosso programa executaria as instruções em `case 2:` mesmo que o usuário fornecesse os valores `0` ou `1`:

```
:~$ php sw02.php 0
0, 1 ou 2
:~$ php sw02.php 1
0, 1 ou 2
:~$ php sw02.php 2
0, 1 ou 2
:~$ php sw02.php 3
3
:~$ php sw02.php 4
qualquer coisa!
```

> **Atenção!** Apesar de também ser uma estrutura de decisão, no PHP a cláusula `switch/case` é considerada uma estrutura de repetição. Então, intruções como `break` e `continue` irão afetar apenas o contexto do próprio bloco `switch`. Isso ajuda muito a entender as peculiaridades do `switch` em comparação aos blocos `case` de outras linguagens e é especialmente importante quando um `switch` for utilizado dentro de uma estrutura de repetição (como um `while`, por exemplo).

Outro ponto importante sobre as cláusulas `case`, é que elas também podem receber expressões em vez de valores literais. Isso poderia resolver o fato das comparações serem feitas de forma flexível (`==`):

**Exemplo sw03.php**

```
$a = false;

switch (true) {
    case ($a === 0):
        echo "O valor é zero.\n";
        break;
    case ($a === ""):
        echo "A string é vazia.\n";
        break;
    case ($a === false):
        echo "O valor é FALSE!\n";
        break;
}
```

Neste exemplo, a expressão em `switch` é o valor booleano literal `true`, mas ele não está sendo comparado com nenhum dos valores dos blocos `case`. A comparação aqui é feita em cada uma das cláusulas, onde nós utilizamos o operador `===` para garantir que `$a` fosse comparado de forma estrita (levando em conta os tipos) com os valores em questão, todos eles interpretados normalmente como `false` pelo PHP.

> **Um questionamento:** Realmente, é uma solução, funciona. Contudo, será que não deveríamos optar por outra estrutura (`if/elseif`) neste caso? Afinal, toda a vantagem do `switch` sobre o seu equivalente implementado com `if/elseif` é o fato dele avaliar a expressão apenas uma vez.

## 6.3 - O operador 'goto'

O operador `goto` é utilizado para fazer o fluxo de execução saltar de um ponto para outro no código. O ponto de destino é definido com um *label* (rótulo) seguido de dois pontos:

```
goto a;

echo "Não tá me vendo aqui?!\n";

a:

echo "A fila andou, meu cara!\n"
```

A implementação do `goto` no PHP não é irrestrita e deve seguir algumas regras:

* O rótulo não pode ser uma variável.
* O rótulo de destino tem que estar no mesmo arquivo e no mesmo contexto do `goto`.
* Portanto, não podemos saltar para dentro do contexto de uma função, por exemplo, e nem sair dela!
* Também não podemos saltar para o interior de uma estrutura de repetição (inclusive um `switch`, nesse sentido).
* Mas podemos utilizar o `goto` para sair de loops e do `switch`.

Por outro lado, nós podemos agrupar blocos de instruções com chaves e identificá-los com rótulos, mas isso não impede que as instruções sejam executadas!

```
a: {
    echo "banana\n";
    echo "laranja\n";
    goto b;
}

echo "Isso não será lido!\n";

b:

exit(0);

```

Repare que o label `a:` nunca foi chamado, mas ainda teríamos na saída....

```
banana
laranja
```

As possibilidades do `goto` são muitas, e ele pode ser muito útil, especialmente para sairmos do switch ou de um loop (ou de ambos aninhados) sem utilizarmos vários `break`:

**Exemplo `sw04.php`**

```
if (!isset($argv[1])) goto d;

switch($argv[1]) {
    case 1:
        goto a;
    case 2:
        goto b;
    default:
        goto c;
}

a: {
    echo "banana\n";
    echo "laranja\n";
    goto c;
}

b: {
    echo "limão\n";
    echo "abacaxi\n";
    goto c;
}


c:

exit(0);

d:

echo "Nenhum parâmetro passado!\n";

goto c;
```

Também é possível implementar um loop com o operador `goto`, o que deve ser usado com cautela -- se não houver uma condição de saída mandando o fluxo de execução para outro ponto no código, o loop será infinito. Por exemplo, isso causaria um loop infinito:

```
$a=0;
a:
echo ++$a.PHP_EOL;
goto a;
```

Com alguma condição de saída, nós podemos controlar o loop, mas só podemos utilizar outro `goto`para isso:

```
$a=0;
a:
echo ++$a.PHP_EOL;
if ($a == 10) goto b;
goto a;
b:
```

## 6.4 - O loop 'for'

Apesar de ter sido bastante utilizado em exemplos anteriores, chegou a hora de conhecer um pouco melhor o loop `for`. No PHP, ele se comporta como seu equivalente na linguagem C, inclusive na sintaxe:

```
for (EXPRESSÃO1; EXPRESSÃO2; EXPRESSÃO3) {
    INSTRUÇÕES
}
```

No começo do loop, a EXPRESSÃO1 é executada uma vez incondicionalmente. Antes de cada iteração EXPRESSÃO2 é avaliada, e o bloco de instruções só é executado se a avaliação resultar em `true`. Caso contrário, o loop é encerrado. A EXPRESSÃO3, por sua vez, só é executada no final de cada iteração.

Exemplo:

```
for ($n = 0; $n < 10; $n++) {
    echo $n;
}
```

* **1**: `$n` recebe o valor `0`;
* **2**: A expressão `$n < 10` é avaliada;
* **3**: Sendo `true`, a instrução `echo $n` é executada;
* **4**: O valor de `$n` passa a ser `1`;
* **5**: A expressão `$n < 10` é avaliada novamente;

E o ciclo continua até que o valor da expressão `$n < 10` seja falso.

Além disso, qualquer uma das 3 expressões pode conter várias expressões separadas por vírgula ou pode até ser omitida, permitindo que o loop `for` emule o comprtamento de outras estruturas e conjuntos de instruções:

**Exemplo 1 - sem a primeira expressão:**

```
php > // Observe que o ';' ainda precisa ser escrito! 
php > $n = 1;
php > for (; $n <= 10; $n++) { echo "$n "; }
1 2 3 4 5 6 7 8 9 10 
```

**Exemplo 2 - sem a segunda expressão (loop infinito):**

```
php > // Observe a instrução de saída `break'...
php > for ($n =1;;$n++) { echo "$n "; if ($n == 10) break; }
1 2 3 4 5 6 7 8 9 10 
```

**Exemplo 3 - sem a terceira expressão:**

```
php > // Observe o incremento na última instrução...
php > for ($n =1;$n <= 10;) { echo "$n "; $n++; }
1 2 3 4 5 6 7 8 9 10 
```

**Exemplo 4 - sem as expressões (outro loop infinito):**

```
php > $n = 1;
php > for (;;) { echo "$n "; $n++; if ($n > 10) break; }
1 2 3 4 5 6 7 8 9 10 
```

**Exemplo 5 - sem o bloco de instruções:**

```
php > for ($x=0, $y=1; $x < 10; $x+=$y, print "$x ");
1 2 3 4 5 6 7 8 9 10 
```

O loop for também aceita a sintaxe alternativa sem chaves utilizando o delimitador `endfor;`:

```
for (EXPRESSÃO1; EXPRESSÃO2; EXPRESSÃO3):
    INSTRUÇÕES
endfor;
```

**Importante!** Após qualquer loop, as variáveis utilizadas para controlar o fim da iteração continuarão disponíveis e com o valor recebido na última iteração:


```
php > for ($n = 1; $n <= 10; $n++) { echo "$n "; }
1 2 3 4 5 6 7 8 9 10
php > echo $n;
11
```

## 6.5 - O loop 'while'

Uma estrutura de repetição bem mais simples (na verdade, a mais simples de todas) é o loop `while`, que também se comporta como o seu equivalente em C:

```
while (EXPRESSÃO) {
    INSTRUÇÕES
}
```

O `while` faz com que o conjunto de instruções seja executado **enquanto** (*while*) EXPRESSÃO for avaliada como verdadeira, o que é testado sempre no início de cada ciclo de iteração. Portanto, mesmo que o valor de EXPRESSÃO mude para `false` durante a execução do bloco de instruções, o loop só será interrompido após o fim da iteração corrente.

**Nota:** EXPRESSÃO tem que ser avaliada como `true` para o loop poder começar.

Nós podemos implementar um loop `while` infinito com qualquer expressão que retorne sempre verdadeiro, inclusive com o valor booleano `true`.

**Exemplo 1:**

```
php > $n = 1;
php > while ($n <= 10) { echo "$n "; $n++; }
1 2 3 4 5 6 7 8 9 10
```

**Exemplo 2 (loop infinito):**

```
php > $n = 1;
php > while (true) { echo "$n "; $n++; if ($n > 10) break; }
1 2 3 4 5 6 7 8 9 10
```

O loop `while` também aceita a sintaxe alternativa sem chaves e com o delimitador `endwhile;`:

```
while (EXPRESSÃO):
    INSTRUÇÕES
endwhile;
```

## 6.6 - O loop 'do/while'

O loop `do/while` é idêntico ao loop `while` com a diferença do momento da avaliação de EXPRESSÃO, que é feita ao final da iteração.

```
do {
    INSTRUÇÕES
} while (EXPRESSÃO);
```

Como consequência, pelo menos a primeira iteração sempre irá ocorrer.

Essa característica era muito usada antes do PHP 5.3 (na verdade, ainda é) para encapsular um trecho do código, interromper o loop `do/while` e saltar para as instruções seguintes. Hoje, após a implementação do operador `goto`, isso deixou de ser necessário:

**Exemplo com `do/while`**

```
// $a = 1;
do {

    if ($a > 5) break;

    // OUTRAS INSTRUÇÕES

} while (false);

// Segue o código...
```

**Exemplo com `goto`**

```
// $a = 1
if ($a > 5) goto segue;

// OUTRAS INSTRUÇÕES

segue:

// Segue o código...

```

## 6.7 - Interrompendo loops

Como já vimos, a instrução `break` interrompe a execução das estruturas:

* `for`
* `foreach`
* `while`
* `do/while`
* `switch`

O que não vimos, porém, é que o `break` aceita um argumento numérico opcional que permite especificar o nível da estrutura aninhada que deve ser interrompida. Se não for informado, o nível será sempre `1`, o que corresponde à estrutura em que o `break` está sendo executado. Por exemplo:

```
while (true) {
    $r = readline("Digite algo: ");
    switch($r) {
        case 1:
            $opt = 'banana';
            break;
        case 2:
            $opt = 'laranja';
            break;
        default:
            echo "Nenhuma escolha foi feita.\n";
            break 2;
    }
    echo "A opção foi: $opt\n";
}
```

Aqui, se o usuário digitar `1` ou `2`, a variável `$opt` receberá um valor e o `break` (sem argumento) fará com que o `switch` seja interrompido e a instrução seguinte seja executada. Mas, se o usuário digitar qualquer outra coisa, uma mensagem é exibida e todo o loop `while` infinito é interrompido com a instrução `break 2`.

**Nota:** Desde o PHP 5.4 não é mais possível passar o argumento numérico do `break` por uma variável (`break $nivel`).

## 6.8 - Saltando o restante da iteração

Outra instrução bastante utilizada para controlar estruturas de loop é o `continue`, que faz com que todas as instruções subsequentes sejam ignoradas e a próxima iteração seja executada:

```
for ($n = 0; $n <= 10; $n++) {
    if (($n % 2) == 0) continue;
    echo "$n ";
}
```

O exemplo irá exibir apenas os números ímpares entre `0` e `10`, já que o `continue` impedirá a execução da instrução `echo "$n "` sempre que o módulo da divisão de `$n` por `2` for `0`:

```
1 3 5 7 9
```

Tal como o `break`, o `continue` também aceita um argumento numérico para especificar quantos níveis de estruturas aninhadas devem ser saltadas.

