# 2 - Interagindo com o shell

Neste módulo, nós veremos as principais formas de exibir saídas e obter dados a partir da linha de comando do terminal, bem como podemos utilizar os comandos e utilitários disponíveis no shell em nossos scripts. Antes porém, precisamos conversar sobre alguns conceitos interessantes da linguagem PHP.

## 2.1 - Instruções, funções e construtores de linguagem

No PHP, cada linha de código terminada com `;` é uma *instrução*. Por analogia, se imaginarmos que estamos trabalhando no terminal, uma instrução pode ser comparada a um comando a ser executado ao teclarmos `[Enter]`.

Quando escrevemos um conjunto de instruções a serem interpretadas e executadas em sequência pelo PHP, nós temos um *script*. Contudo, dentro do próprio script nós podemos criar blocos de instruções e dar nomes a eles. Esses blocos não serão executados enquanto não forem chamados pelos seus nomes -- são as *funções*.

Além disso, toda função tem a característica de permitir que sejam passadas informações que irão parametrizar a sua execução. No PHP, esses parâmetros são passados para a função entre parêntesis. Então, mesmo que a função não espere parâmetros (também chamados popularmente de "argumentos"), os parêntesis sempre deverão ser usados. Por exemplo:

```
php > printf("Olá, mundo!");
Olá, mundo!
```

Aqui, `printf` é uma função que exige pelo menos um parâmetro entre os parêntesis (no caso, `"Olá, mundo!"`). Se uma função exige parâmetros e eles não forem passados, o PHP retornará um erro:

```
php > printf();
PHP Warning:  printf() expects at least 1 parameter, 0 given in php shell code on line 1
```

Já a função `time`, abaixo, não requer a passagem de um parâmetro mas, mesmo assim, deve ser chamada com os parêntesis:

```
php > echo time();
1591363314
```

> **Nota:** o PHP vem por padrão com várias funções internas para as mais diversas finalidades. Deste modo, nós podemos chamar essas funções para realizar uma infinidade de tarefas no nosso código.

Já os **construtores de linguagem** são elementos criados para se comportarem como funções, mas são tratados pelo interpretador de uma forma bem diferente.

Basicamente, os construtores de linguagem são reconhecidos automaticamente pelo PHP como um elemento da própria linguagem sem que seja feita uma análise da estrutura da instrução, o que torna a sua execução mais rápida.

Outra característica dos construtores de linguagem que os difere das funções, é que eles **nem sempre** exigem parêntesis quando são chamados, como é o caso do `echo`:

```
php > echo "Olá, mundo!";
Olá, mundo!
php > echo ("Olá, mundo!");
Olá, mundo!
php > echo("Olá, mundo!");
Olá, mundo!
```

Mas, **preste atenção**:

* Alguns construtores de linguagem dispensam os parêntesis.
* Outros exigem.
* Alguns nem podem ser usados com parêntesis!
  
Resumindo...
  
* Cada caso é um caso.
* Só o manual tem a resposta certa.
* O importante é saber quando estamos usando uma função ou um construtor de linguagem.

## 2.2 - Exibindo saídas no terminal

Como veremos, todos os recursos para exibição de mensagens no terminal são exatamente os mesmos que utilizaríamos para exibir textos em páginas HTML:

* `echo` - exibe uma ou mais strings
* `print` - exibe uma string
* `printf` - exibe strings formatadas
* `print_r` - exibe informações sobre variáveis de forma legível

Todas essas funções (`printf` e `print_r`) e construtores de linguagens (`echo` e `print`) exibirão informações na saída, seja ela o terminal ou um documento HTML gerado dinamicamente no servidor web.

> **Nota:** uma *string* é uma cadeia de caracteres.

### 'echo' e 'print'

Os construtores `echo` e `print` fazem exatamente a mesma coisa: exibem uma string na saída. A diferença é que o `echo` pode concatenar várias strings (separadas por vírgulas) passadas como argumentos, enquanto o `print` aceita apenas um argumento.

Observe os exemplos:

```
php > // Usando o 'echo'...
php > echo "banana ", "laranja ", "abacate";
banana laranja abacate
```

```
php > // Usando o 'print'...
php > print "banana laranja abacate";
banana laranja abacate
```

### Concatenação

Tanto o `echo` quanto o `print` podem fazer concatenações (a junção de strings) com o **operador de concatenação** `.` (ponto):

```
php > print "banana"." "."laranja";
banana laranja
php > echo "banana"." "."laranja";
banana laranja
```

### Quebra de linha

Nenhum dos dois construtores gera uma quebra de linha após a string resultante, o que pode ser resolvido com a concatenação do caractere de controle `\n` ou do retorno da função `chr()`:

```
php > echo "banana"."\n"."laranja";
banana
laranja
php > echo "banana".chr(10)."laranja";
banana
laranja
```

### Aspas duplas e simples

As aspas duplas nos permitem concatenar strings com caracteres de controle e ainda expandem os os valores em variáveis:

```
php > echo "banana\nlaranja\n";
banana
laranja
```

```
php > print "Você gosta de $str1 e $str2?\n";
Você gosta de banana e laranja?
```

Já as aspas simples tratarão tudo em seu interior como caracteres literais:

```
php > print 'Você gosta de $str1 e $str2?\n';
Você gosta de $str1 e $str2?\n
```

### Múltiplas linhas

Se não quisermos utilizar caracteres de controle para quebrar linhas, nós podemos escrever as strings entre aspas duplas ou simples em várias linhas:

```
php > echo '
php ' Qual é
php ' o seu
php ' nome?
php ' ';

Qual é
o seu
nome?
```

> **Nota:** observe que as outras diferenças entre as aspas simples e duplas ainda continuarão vigorando.

### Concatenando e formatando strings com a função 'printf()'

Outra forma de concatenar strings é com a função `printf()`. 

> **Nota:** por se tratar de uma função incrivelmente versátil e cheia de opções, nós falaremos dela apenas superficialmente neste momento.

Observe o exemplo:

```
php > $str1="banana";
php > $str2="laranja";
php > printf("Você gosta de %s?", $str1);
Você gosta de banana?
php > printf("Não, eu prefiro %s.", $str2);
Não, eu prefiro laranja.
```

Nele, o `%s` é chamado de **especificador de formato**. O especificador serve para "guardar o lugar" dos valores que serão informados nos demais argumentos e sempre devem corresponder aos seus tipos. No caso, como o valor será uma string, o especificador é `%s`.

```
php > printf("Eu prefiro %s, mas não odeio %ss.", $str2, $str1);
Não, eu prefiro laranja, mas não odeio bananas.
```

Se fosse um número inteiro, por exemplo, seria `%d`:

```
php > $nota=10;
php > $aluno="João";
php > printf("%s tirou nota %d", $aluno, $nota);
João tirou nota 10
```

Além disso, os especificadores podem realizar vários tipos de conversões, por exemplo:

```
php > $valor=3.1415169;
php > printf("%.2f", $valor);
3.14
```

O PHP possui funções internas para quase todas as conversões que o `printf()` é capaz de fazer. A questão, porém, é que essas funções não fazem a exibição direta das strings retornadas e dependerão de um `echo` ou de um `print` para isso. Aliás, também existe a função `sprintf()`, idêntica à função `printf()`, mas ela apenas retorna uma string sem fazer a exibição na saída.

### Exibindo variáveis de forma legível com `print_r()`

A função `print_r()` exibe o conteúdo de variáveis de forma humanamente legível.

Ela não parece muito útil quando tentamos exibir o conteúdo de variáveis escalares (nomes que apontam para apenas um valor), mas é muito usada para exibir o conteúdo de veriáveis vetoriais (arrays):

```
php > $alunos=["Maria", "Pedro", "Antonio"];
php > print_r($alunos);
Array
(
    [0] => Maria
    [1] => Pedro
    [2] => Antonio
)
```

Opcionalmente, nós podemos passar um segundo parâmetro para a função `print_r()` indicando se queremos exibir seu retorno ou apenas guardá-lo em uma variável.

Por padrão, este segundo parâmetro vem definido como `false` (ou `0`), o que faz que o retorno seja exibido. Se quisermos que ele seja apenas armazenado, nós precisamos informar o valor `true`:

```
php > $verdade=print_r($alunos[1] == "Pedro", true);
php > echo $verdade;
1
```

## 2.3 - Parâmetros posicionais

Uma das formas de passarmos opções para um programa na linha de comando é através dos **parâmetros posicionais**, que são basicamente aqueles argumentos que escrevemos logo após os nomes dos programas, por exemplo:

```
:~$ mv arquivo1.txt arquivo2.txt
```

Na linha de comando do shell, cada uma dessas palavras separadas pelo espaço será tratada como um parâmetro e será numerada conforme a sua posição (daí "posicional"):

```
mv arquivo1.txt arquivo2.txt
 ^     ^            ^
 |     |            |
 0     1            2
```

O parâmetro `0` sempre será o nome do programa ou do comando, e todos os outros parâmetros serão numerados de acordo com sua ordem de aparição.

Para capturar esses parâmetros posicionais de dentro dos nossos scripts nós utilizamos a array `$argv`. Trata-se de um vetor gerado pelo próprio PHP-CLI quando o script é executado na linha de comando, e ele irá conter o nome do script (no índice `0`) e todos os parâmetros passados na linha de comando.

Por exemplo, no script `teste-pars.php`:

```
#!/usr/bin/env php

<?php

print_r($argv);

?>
```

Tudo que ele fará é exibir o conteúdo da array `$argv`. Então, se nós o executarmos na linha de comando sem argumentos:

```
:~$ ./teste-pars.php 

Array
(
    [0] => ./teste-pars.php
)
```

Como podemos ver, o elemento de índice `0` de `$argv` é o nome do próprio script tal como ele foi chamado na linha de comando. Vamos incluir alguns parâmetros e ver o que acontece:

```
:~$ ./teste-pars.php banana laranja abacate

Array
(
    [0] => ./teste-pars.php
    [1] => banana
    [2] => laranja
    [3] => abacate
)

```

Agora, os valores `banana`, `laranja` e `abacate` foram capturados pelo nosso script na array `$argv` e podem ser usados no nosso código. Outro exemplo, o script `hello.php`:

```
#!/usr/bin/env php

<?php

echo "Olá, $argv[1]!\n\n";

?>
```

Sabendo como funcionam os parâmetros posicionais, nós queremos utilizar o elemento de índice `1` em `$argv` na string que será exibida no terminal:

```
$ ./hello.php Blau

Olá, Blau

```

### Contando o número de parâmetros

Às vezes, é preciso saber quantos parâmetros foram passados na linha de comando, ou mesmo se foi passado o número correto de parâmetros. Para isso, nós podemos contar com a variável `$argc`. Modificando o script `teste-pars.php`:

```
#!/usr/bin/env php

<?php

echo "O número de parâmetros passados é: $argc\n\n";

print_r($argv);

?>
```

Vamos executá-lo novamente:

```
:~$ ./teste-pars.php banana laranja abacate

O número de parâmetros passados é: 4

Array
(
    [0] => ./teste-pars.php
    [1] => banana
    [2] => laranja
    [3] => abacate
)

```

Como podemos observar, o parâmetro `0` (o nome do script) também foi incluído na contagem, e isso deve ser levado em consideração nos nossos códigos.


## 2.4 - Lendo a entrada padrão

Outra forma de obtermos informações do usuário na linha de comando é solicitando que ele digite algo que o nosso script irá processar posteriormente. Isso pode ser feito de várias formas no PHP-CLI, mas no momento vamos nos concentrar na forma mais simples, que é com a função `readline()`.

A função `readline()` irá pausar a execução do script até que o usuário digite alguma coisa e tecle `[Enter]`. Tudo que o usuário digitar (exceto o `[Enter]`) será retornado pela função e poderá ser armazenado numa variável. Por exemplo:

```
php > $var=readline("Digite algo: ");
Digite algo: teste
php > echo $var;
teste
```

O parâmetro opcional de `readline()` é uma string que servirá de orientação sobre o que o usuário deverá informar, ou "*prompt*". No nosso caso, foi `"Digite algo: "`. 

Então, vamos modificar o nosso script `hello.php` para que ele pergunte o nome do usuário:

```
#!/usr/bin/env php

<?php

$nome=readline("Digite o seu nome: ");

echo "Olá, $nome!\n\n";

?>
```

Executando...

```
$ ./hello.php 

Digite o seu nome: Blau Araujo
Olá, Blau Araujo!

```

## 2.5 - Executando comandos externos

Uma das maiores forças do PHP-CLI está na possibilidade de usar recursos do próprio shell nos nossos códigos e interagir com ele.

Por exemplo, se eu estiver no terminal quiser saber o meu nome de usuário, eu executo o comando `whoami`:

```
:~$ whoami
blau
```

O PHP-CLI nos oferece diversas formas de acessar os comandos do shell nos nossos scripts. Neste momento, vamos conhecer apenas as suas principais: as funções `exec()` e `system()`.

As duas fazem a mesma coisa, só que apenas a função `system()` exibe diretamente a saída do comando externo. Por exemplo:

```
php > system("whoami");
blau
php > maquina=exec("hostname");
php > echo $maquina;
enterprise
```

> **Nota:** repare que a linha de comando executada deve estar entre parêntesis.

Aproveitando que aprendemos mais esse recurso do PHP-CLI, vamos incrementar o nosso script `hello.php`:

```
#!/usr/bin/env php

<?php

$nome=readline("Digite o seu nome: ");

echo "Olá, $nome!\n\n";

echo "Informações do Sistema\n\n";

echo "Usuário     : ".exec("whoami")."\n";
echo "Hostname    : ".exec("hostname")."\n";
echo "Tempo ligado: ".exec("uptime -p")."\n";
echo "Kernel      : ".exec("uname -rms")."\n\n";

?>
```

Executando...

```
$ ./hello.php 

Digite o seu nome: Blau Araujo
Olá, Blau Araujo!

Informações do Sistema

Usuário     : blau
Hostname    : enterprise
Tempo ligado: up 1 day, 6 hours, 58 minutes
Kernel      : Linux 4.19.0-9-amd64 x86_64

```