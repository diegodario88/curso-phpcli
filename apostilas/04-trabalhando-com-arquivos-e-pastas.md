# PHP-CLI - Aula 4: Trabalhando com arquivos e pastas

O PHP oferece diversas funções para navegar, listar, criar, ler e alterar arquivos e pastas. Nesta aula, nós tentaremos esboçar uma espécie de guia rápido de consulta, agrupando as principais funções de acordo com as tarefas que quisermos executar.

## 4.1 - Listar diretórios

Existem essencialmente duas formas de lidar com a listagem de diretórios e seus conteúdos: através da criação de um manipulador (*handler*) de diretórios, que é a forma mais tradicional, ou através da função `scandir()`, que retorna a listagem do diretório sob a forma de uma array. 

Obviamente, a função `scandir()` torna o nosso trabalho muito mais fácil, mas a operação com manipuladores pode nos dar mais controle sobre o que realmente queremos de uma listagem de diretórios.

### 4.1.1 - Função 'scandir()'

```
scandir('DIR' [, ORDEM])
```

Retorna uma array com a listagem de pastas e arquivos encontrados em DIR.

**DIR**

É o caminho da pasta que queremos listar.

**ORDEM**

Parâmetro opcional que indica a ordem da listagem:

* **SCANDIR_SORT_ASCENDING** - Alfanumérica ascendente (padrão)
* **SCANDIR_SORT_DESCENDING** - Alfanumérica descendente
* **SCANDIR_SORT_NONE** - Sem ordenação

### 4.1.2 - Listando pastas e arquivos com um manipulador

O processo tradicional de listagem de diretórios envolve três etapas: abrir o diretório associando um manipulador a ele, ler recursivamente o conteúdo do diretório e, ao final, fechar o manipulador criado. De forma geral, este seria o procedimento:

```
$h = opendir(CAMINHO);

while (false !== ($e = readdir($h))) {
    echo $e.PHP_EOL;
}

closedir($h);
```

Repare que o processo envolve três funções:

* `opendir()` - Abre CAMINHO para leitura e retorna um manipulador.
* `readdir()` - Lê uma entrada no diretório listado.
* `closedir()` - Fecha o manipulador. 

A cada chamada da função `readdir()` o ponteiro da listagem passa para a entrada seguinte no diretório, por isso é comum que ela seja utilizada dentro de um laço de repetição, o que permite uma leitura sequencial de cada entrada. 

Também é importante notar que `opendir()` e `readdir()` retornam `FALSE` caso haja algum problema, e isso pode ser usado para determinar, por exemplo, se CAMINHO não existe ou está inacessível, ou se já não há mais entradas no diretório a serem listadas. Portanto, a forma mais comum de listar o conteúdo de uma pasta seria:

```
// Se 'opendir()' retornar FALSE, nada é executado...
if ($h = opendir(CAMINHO)) {

    // Se 'readdir()' retornar o tipo booleano FALSE, o loop para...
    while (false !== ($e = readdir($h))) {
        echo $e.PHP_EOL;
    }

    closedir($h);
}
```

> **Nota:** O operador de comparação `!==` avalia como verdadeiro se ambos os termos forem diferentes em valor e tipo.

## 4.2 - Buscar arquivos e pastas segundo um padrão

O processo de listar arquivos e pastas filtrando o resultado através de padrões é chamado de **globbing**, e o PHP tem uma função para lidar justamente com isso, a função `glob()`, que retorna uma array com os resultados encontrados.

```
glob('PADRÃO' [, FLAGS])
```

Retorna uma array com os resultados que correspondam a PADRÃO.

**PADRÃO**

O padrão de filtragem é uma string que utiliza basicamente os mesmos símbolos (metacaracteres) utilizados na linha de comando do terminal.

**METACARACTERES**

* `*` - Zero ou mais caracteres quaisquer
* `?` - Exatamente um caractere
* `[LISTA]` - Um caractere existente na lista
* `[!LISTA]` - Qualquer caractere fora da lista

Além disso, podemos escapar os caracteres que devem ser considerados literais na busca com a barra invertida (`\`).

**FLAGS**

Também podemos modificar o comportamento padrão da função incluindo FLAGS:

* **GLOB_MARK** - Inclui uma barra no resultado se for diretório.
* **GLOB_NOSORT** - Não ordena resultados.
* **GLOB_NOCHECK** - Retorna PADRÃO se não hover correspondência.
* **GLOB_NOESCAPE** - A barra invertida passa a ser literal.
* **GLOB_BRACE** - Habilita a "lista  ou" entre chaves.
* **GLOB_ONLYDIR** - Lista apenas diretórios.
* **GLOB_ERR** - Para a busca se houver erros.

## 4.3 - Comparar nomes de arquivos com um padrão

Quando não temos um nome exato para fazer uma comparação, é muito útil termos a alternativa de fazer a comparação através de um padrão, e é para isso que serve a função `fnmatch()`.

```
fnmatch('PADRÃO', 'NOME'[, FLAGS])
```

Retorna `TRUE` ou `FALSE` como resultado da comparação de PADRÃO com NOME.


**PADRÃO**

Uma string de comparação usando os mesmos metacaracteres da função `glob()`.

**NOME**

Nome do arquivo que está sendo comparado.

**FLAGS**

* **FNM_NOESCAPE** - A barra invertida passa a ser literal.
* **FNM_PATHNAME** - A barra normal em NOME só bate com a barra normal em PADRÃO.
* **FNM_PERIOD** - Ponto no início de NOME tem que bater com um ponto em PADRÃO.
* **FNM_CASEFOLD** - Ignora caixa alta ou baixa.

## 4.4 - Testar atributos de arquivos

O PHP oferece várias funções para testar atributos de arquivos, todas retornando `TRUE` caso o teste tenha uma correspondência verdadeira com o atributo testado.

* `is_dir('ARQUIVO')` - TRUE se for diretório
* `is_file('ARQUIVO')` - TRUE se for arquivo comum
* `is_link('ARQUIVO')` - TRUE se for um link simbólico
* `is_executable('ARQUIVO')` - TRUE se for executável
* `is_readable('ARQUIVO')` - TRUE se for possível ler o arquivo
* `is_writable('ARQUIVO')` - TRUE se for possível escrever no arquivo
* `file_exists('ARQUIVO')` - TRUE se ARQUIVO existir

> **Nota:** Lembre-se de que, no sistema de arquivos, tudo é arquivo, inclusive as pastas.

## 4.5 - Obtendo informações de caminhos

No PHP, é possível obter partes de um caminho que correspondam ao nome de um arquivo ou uma pasta, o diretório onde ele está, a extensão do arquivo ou o nome sem a extensão.

### 4.5.1 - Função 'getcwd()'

```
getcwd()
```

Retorna o caminho completo do diretório de trabalho corrente.

### 4.5.2 - Função 'dirname()'

```
dirname('CAMINHO'[, NÍVEIS])
```

Dado um CAMINHO, retorna tudo até antes da sua última parte, seja ela uma pasta ou um nome de arquivo:

```
php > echo dirname('/home/user/teste/');
/home/user

php > echo dirname('/home/user/teste/arquivo.php');
/home/user/teste
```

Opcionalmente, podemos definir até quantos níveis antes da parte final nós queremos que a função retorne (o padrão é 1).

```
php > echo dirname('/home/user/teste/', 2);
/home

php > echo dirname('/home/user/teste/arquivo.php', 2);
/home/user
```

### 4.5.3 - Função 'basename()'

```
basename('CAMINHO'[, 'SUFIXO'])
```

Retorna a última parte de CAMINHO.

```
php > echo basename('/home/user/teste/');
teste

php > echo basename('/home/user/teste/arquivo.php');
arquivo.php
```

Opcionalmente, podemos eliminar um SUFIXO da string retornada:

```
php > echo basename('/home/user/teste/arquivo.php', '.php');
arquivo
```

### 4.5.4 - Função 'pathinfo()'

```
pathinfo('CAMINHO'[, OPÇÃO])
```

Retorna uma array com  informações de CAMINHO.

```
php > print_r (pathinfo('/home/user/teste/arquivo.php'));
Array
(
    [dirname] => /home/user/teste
    [basename] => arquivo.php
    [extension] => php
    [filename] => arquivo
)
```

**OPÇÕES**

Opcionalmente, podemos especificar as informações que serão retornadas.

* **PATHINFO_DIRNAME** - Equivale à função `dirname()`.
* **PATHINFO_BASENAME** - Equivale à função `basename()`.
* **PATHINFO_FILENAME** - Retorna nome do arquivo sem a extensão.
* **PATHINFO_EXTENSION** - Retorna apenas a extensão.

## 4.6 - Operações comuns com arquivos

Também podemos realizar as mesmas operações mais simples com pastas e arquivos que executamos na linha de comando de um terminal.

### 4.6.1 - Função 'chdir()'

```
chdir('DIR')
```

Altera o diretório de trabalho para DIR.

### 4.6.2 - Função 'copy()'

```
copy('ORIGEM', 'DESTINO')
```

Copia arquivos e pastas em ORIGEM para DESTINO.

### 4.6.3 - Função 'rename()'

```
rename('ORIGINAL', 'NOVO')
```

Move arquivos e pastas descritos em ORIGINAL para o local ou o nome NOVO.

### 4.6.4 - Função 'unlink()'

```
unlink('ARQUIVO')
```

Remove arquivos e pastas.

### 4.6.5 - Função 'chmod()'

```
chmod('ARQUIVO', PERMISSÂO)
```

Atribui a ARQUIVO uma PERMISSÃO em formato octal.


## 4.7 - Leitura e escrita de arquivos

Assim como o trabalho com diretórios, o PHP oferece duas abordagens para lidar com a escrita e leitura de arquivos: a tradicional, com a criação de um manipulador, ou com o uso de várias outras funções mais objetivas -- porém, menos flexíveis.

### 4.7.1 - Manipulação básica de arquivos

O processo básico de manipulação básica de arquivos envolve, como antes, três estágios: a abertura do arquivo para uma finalidade (leitura, escrita, etc...) com a criação de um manipulador, a leitura ou a escrita no arquivo, e o fechamento do arquivo e seu manipulador.

A função utilizada para abrir o arquivo e retornar um manipulador é a `fopen()`, e todo arquivo aberto deve ser fechado ao final das operações com a função `fclose()`.

```
// Abre arquivo no modo MODO...
$h = fopen('ARQUIVO', 'MODO');

... INSTRUÇÕES ...

// Fecha o manipulador do arquivo...
fclose($h);
```

Os modos são definidos por uma string que indica:

* **'r'** - Abre somente para leitura e coloca o ponteiro do arquivo no começo do arquivo.

* **'r+'** - Abre para leitura e escrita e coloca o ponteiro do arquivo no começo do arquivo.

* **'w'** - Abre somente para escrita, coloca o ponteiro do arquivo no começo do arquivo e reduz o comprimento do arquivo para zero -- se o arquivo não existir, tenta criá-lo.

* **'w+'** - Abre para leitura e escrita, coloca o ponteiro do arquivo no começo do arquivo e reduz o comprimento do arquivo para zero -- se o arquivo não existir, tenta criá-lo.

* **'a'** - Abre somente para escrita e coloca o ponteiro do arquivo no final do arquivo -- se o arquivo não existir, tenta criá-lo.

* **'a+'** - Abre para leitura e escrita e coloca o ponteiro do arquivo no final do arquivo -- se o arquivo não existir, tenta criá-lo.

* **'x'** - Cria e abre o arquivo somente para escrita e coloca o ponteiro no começo do arquivo. Se o arquivo já existir, a chamada a fopen() falhará, retornando FALSE e gerando um erro E_WARNING. Se o arquivo não existir, tenta criá-lo.

* **'x+'** - Cria e abre o arquivo para leitura e escrita e coloca o ponteiro no começo do arquivo. Se o arquivo já existir, a chamada a fopen() falhará, retornando FALSE e gerando um erro E_WARNING. Se o arquivo não existir, tenta criá-lo. 

#### Funções para leitura

```
fread(MANIPULADOR, BYTES)
```

Lê uma quantidade de BYTES a partir do ponteiro indicado pelo MANIPULADOR ou até chegar ao fim do arquivo ou encontrar pacotes de dados incompletos ou truncados.

```
fgets(MANIPULADOR[, BYTES])
```

Lê uma linha a partir do ponteiro indicado pelo MANIPULADOR ou até encontrar: uma quebra de linha, a quantidade de BYTES menos 1, ou o fim do arquivo.

```
fgetc(MANIPULADOR)
```

Lê apenas um caractere a partir do ponteiro indicado pelo MANIPULADOR.

#### Funções para escrita

```
fwrite(MANIPULADOR, 'STRING'[, BYTES ])
```

Escreve o conteúdo de STRING na posição do ponteiro indicado pelo MANIPULADOR. Opcionalmente, a escrita pode ser interrompida após um dado número de BYTES.

### 4.7.2 - Lendo e escrevendo em arquivos do jeito fácil

Especialmente quando queremos todo o contéudo de um arquivo, ou escrever conteúdos todos de uma vez, é muito mais prático utilizar as funções abaixo.

### Função 'touch()'

```
touch('ARQUIVO')
```

Altera data e hora de acesso a ARQUIVO ou o cria se ele não existir.

### Função 'file()'

```
file('ARQUIVO'[, FLAGS])
```

Retorna uma array com as linhas de ARQUIVO. Opcionalmente, podemos alterar o comportamento padrão da função com algumas FLAGS, como:

* **FILE_IGNORE_NEW_LINES** - Ignora quebras de linha.
* **FILE_SKIP_EMPTY_LINES** - Pula linhas vazias.

### Função 'file_get_contents()'

```
flie_get_contents('ARQUIVO')
```

Retorna todo o conteúdo de ARQUIVO como uma string.

### Função 'file_put_contents()'

```
file_put_contents('ARQUIVO', 'DADOS'[, FLAGS])
```

Escrve todo o conteúdo de DADOS em ARQUIVO. Se ARQUIVO não existir, ele é criado e DADOS são escritos nele.

**DADOS**

O conteúdo a ser escrito no arquivo. Pode ser uma string, uma array ou um stream de dados.

**FLAGS**

* **FILE_APPEND** - Se ARQUIVO existir, inclui DADOS no final do conteúdo.
* **LOCK_EX** - Tranca o arquivo durante a escrita.


## 4.8 - Streams de entrada e saída

Como o nosso foco é o PHP-CLI e o desenvolvimento de aplicações em PHP para a linha de comando, não poderíamos deixar de falar da leitura e escrita nos três principais descritores de arquivos, os quais representam os streams de entrada e saída: `STDIN`, `STDOUT` e `STDERR`.

Para o kernel Linux (e todos os sistemas *Unix like*), tudo é arquivo, inclusive dispositivos físicos, como os nossos teclados, monitores e os fluxos de dados entre o kernel e o usuário. Quando iniciado, o kernel disponibiliza três descritores de arquivos (FD) especiais:

| FD | Nome   | Função                                                                         |
|----|--------|--------------------------------------------------------------------------------|
| 0  | STDIN  | Capturar todo o fluxo de dados digitado no terminal (entrada padrão).          |
| 1  | STDOUT | Enviar fluxos de dados para a exibição no terminal (saída padrão).             |
| 2  | STDERR | Enviar fluxos de dados contendo erros para o terminal (saída padrão de erros). |

O PHP-CLI traz definidas algumas constantes para facilitar a programação envolvendo os fluxos de entrada e saída:

| Constante | Descrição                              |
|-----------|----------------------------------------|
| `STDIN`   | Um stream aberto para a entrada padrão |
| `STDOUT`  | Um stream aberto para a saída padrão   |
| `STDERR`  | Um stream aberto para a saída de erros |

Portanto, é possível ler e escrever nas nossas próprias cópias dos respectivos descritores de arquivos utilizando essas constantes nos nossos códigos. Por exemplo:


### Lendo a entrada padrão

```
echo 'Digite o seu nome: ';
$nome = trim(fgets(STDIN));
printf("Seu nome é %s." $nome);
```

### Escrevendo na saída padrão

```
$msg = "Olá, mundo!\n";
fwrite(STDIN, $msg);
```

### Escrevendo na saída e erros

```
$msg = "Olá, mundo!\n";
fwrite(STDERR, $msg);
```

> **Nota:** Os sistemas *Unix like* replicam fluxos de erro para a saída padrão.

Embora práticas, essas constantes podem apresentar problemas (especialmente `STDIN`) em algumas situações. Observe o exemplo:

```
echo 'Caractere 1: '; 
$a = fgetc(STDIN);

echo 'Caractere 2: ';
$b = fgetc(STDIN); 
```

Quando executado, este código irá pausar a execução para pedir a entrada de um único caractere, o que acontecerá com a chamada da primeira linha `fgetc(STDIN)`. Mas, ao teclarmos `Enter`, nós estamos enviando mais um caractere junto com aquele que digitamos, o que fará com que o `fgetc(STDIN)` seguinte seja "pulado".

Isso acontece porque a constante `STDIN` estará apontando sempre para o mesmo *handler* do descritor de arquivos durante a execução do script:

```
echo 'Caractere 1: '; 
$a = fgetc(STDIN);

echo 'Caractere 2: ';
$b = fgetc(STDIN); 

echo "1: $a\n2: $b\n";
```

Executando...

```
:~$ php streamio-4.php 
Caractere 1: ab
Caractere 2: 
1: a
2: b
```

## 4.8.1 - Abrindo fluxos de entrada e saída com 'php://'

Na verdade, o que as constantes de stream de entrada e saída fazem é retornar um manipulador (*handler*) para o descirtor de arquivos de um fluxo de dados. Elas armazenam o equivalente a...

| Constante | Instrução                   |
|-----------|-----------------------------|
| `STDIN`   | `fopen('php//stdin', 'r')`  |
| `STDOUT`  | `fopen('php//stdout', 'w')` |
| `STDERR`  | `fopen('php//stderr', 'w')` |

Ou seja, um *handler* para o descritor de arquivos é criado, mas não é fechado enquanto o script não terminar a sua execução. Portanto, para evitar surpresas, pode ser mais insteressante realizar todo o processo de abertura e fechamento do descritor de arquivos:

```
function readchar($prompt) {
    $h = fopen('php://stdin', 'r');
    echo $prompt;
    $input = fgetc($h);
    fclose($h);
    return $input;
}


$a = readchar('Caractere 1: ');
$b = readchar('Caractere 2: ');

echo "\n1: $a\n2: $b\n";
```

Executando...

```
:~$ php streamio-5.php 
Caractere 1: ab
Caractere 2: c

1: a
2: c
```
