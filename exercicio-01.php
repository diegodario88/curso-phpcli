#!/usr/bin/env php

<?php
$sorteios = file('lotomania.txt', FILE_IGNORE_NEW_LINES);
system('clear');
?>
CURSO PHP-CLI - Exercício #1
-----------------------------------------------------------
Dezenas em ordem de sorteio:
-----------------------------------------------------------
<?php include 'lotomania.txt'; ?>
-----------------------------------------------------------
Dezenas em ordem crescente:
-----------------------------------------------------------
<?php
foreach ($sorteios as $linha) {
    $dezenas = explode(' ', $linha);
    sort($dezenas);
    $ordenadas = implode(' ', $dezenas);
    echo $ordenadas."\n";
}
?>
-----------------------------------------------------------
Dezenas em ordem crescente com '00' por último:
-----------------------------------------------------------
<?php
foreach ($sorteios as $linha) {
    $dezenas = explode(' ', $linha);
    sort($dezenas);
    if ($dezenas[0] == '00') {
        // Elimina o primeiro elemento e reindexa...
        array_shift($dezenas);
        // Inclui um elemento no fim da array...
        array_push($dezenas, '00');
    }
    $ordenadas = implode(' ', $dezenas);
    echo $ordenadas."\n";
}
?>
-----------------------------------------------------------
